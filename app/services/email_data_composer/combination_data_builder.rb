# frozen_string_literal: true

module EmailDataComposer
  # Returns the data of a combination (if part of data doesn't exist, it is replaced with
  # same data of the base product)
  # @attr [Hash] product_data the data of the product returned by Ecwid
  # @attr [Hash] combination_id The id of the combination, that the data should be combined for
  # @attr [Hash] merchant_settigns the settings of the store in Ecwid
  # @see
  #   MerchantData Merchant Data class
  class CombinationDataBuilder
    def initialize(product_data, combination_id, merchant_settings)
      @product_data      = product_data
      @combination_id    = combination_id
      @merchant_settings = merchant_settings
    end

    # Returns the hash of needed combination data
    # @example
    #   EmailDataComposer::BaseProductDataBuider.new(product_data, combination_id, merchant_settings).call

    def call
      combination = find_combination
      price_num   = combination['price'] || @product_data['price']
      {
        'title' => @product_data['title'],
        'product_url' => @product_data['product_url'],
        'price' => product_price(price_num),
        'image_url' => combination['smallThumbnailUrl'] ||
          @product_data['image_url'],
        'options' => draw_options_table(combination['options'])
      }
    end

    private

    attr_accessor :product_data, :merchant_settings

    # Returns the data of the combination that is requested
    # by comparing its ID with the IDs of all presented options
    # @return [Hash] combination data
    # @see
    #   https://developers.ecwid.com/api-documentation/product-variations Ecwid doc

    def find_combination
      product_data['combinations'].detect do |combo|
        combo['id'] == @combination_id
      end
    end

    # Generates HTML Code for the product options table
    # So that each option stands on its own line
    # @param options [Hash] hash of options (names and values)
    # @return [String] HTML code for options

    def draw_options_table(options)
      options.each_with_object([]) do |option, str|
        str << "<tr><td><strong>#{option['name']}</strong>:"\
               " #{option['value']}</td></tr>"
      end.join
    end

    # Ecwid provides price of a product as int
    # Currency prefix and suffix are provided in merchant settings
    # @param price [Integer] price as int
    # @return [String] price with suffix and prefix
    # @example
    #   price(100)
    #   #> $100 USD

    def product_price(price)
      merchant_settings['currency_prefix'] +
        price.to_s +
        merchant_settings['currency_suffix']
    end
  end
end
