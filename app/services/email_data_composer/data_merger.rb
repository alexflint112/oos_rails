# frozen_string_literal: true

module EmailDataComposer
  # Merges the Email Data into a single hash
  # @attr [Integer] store_id ID of the store
  # @attr [Integer] product_id ID of the requested product
  # @attr [Hash]    store_seting the settings of the store in the app
  # @attr [Hash]    product_data the data of the product returned by Ecwid
  # @attr [Hash]    merchant_settings the settings of the merchant's store in Ecwid
  # @attr [Hash]    request the request itself from db
  class DataMerger
    def initialize(store_id, product_id, store_setting, product_data, merchant_settings, request)
      @store_id = store_id
      @product_id        = product_id
      @combination_id    = request.combination_id
      @store_setting     = store_setting
      @request           = request
      @product_data      = product_data
      @merchant_settings = merchant_settings
    end

    # Merges the data together
    # @return [Hash] the data for NotificationMailer to send email notification
    def call
      result = [subject_and_content, product_data_for_mail, from_and_to_emails]
      result.inject(&:merge)
    end

    private

    # Prepares email addresses for the mailer
    # @return [Hash] from and to email addresses
    def from_and_to_emails
      address = @merchant_settings['to_address']
      checked_address =
        EmailDataComposer::AddressChecker.new(address).call
      {
        'from': checked_address,
        'to': @request.email
      }
    end

    # Prepares subject and content
    # @return [Hash] subject and content
    def subject_and_content
      EmailDataComposer::ContentsData.new(
        @store_setting,
        @request.name
      ).call
    end

    # Depending on whether base product or combination is requested,
    # prepares corresponding data
    # @return [Hash] product data for mail
    def product_data_for_mail
      if @combination_id == 1
        EmailDataComposer::BaseProductDataBuilder.new(
          @product_data,
          @merchant_settings
        ).call
      else
        EmailDataComposer::CombinationDataBuilder.new(
          @product_data,
          @combination_id,
          @merchant_settings
        ).call
      end
    end
  end
end
