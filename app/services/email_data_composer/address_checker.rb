# frozen_string_literal: true

module EmailDataComposer
  # It is forbidden to send emails from the listed domains without permission
  # The service replaces address with the default one
  # if merchant's address is registered under a listed domain
  class AddressChecker
    FORBIDDEN_EMAILS_LIST =
      %w[
        yahoo.fr
        yahoo.com.tw
        hotmail.co.uk
        dayrep.com
        bk.com
        bk.ru
        ymail.com
        yahoo.co.uk
        aol.com
        hotmail.com
        yahoo.com
        yahoo.co.uk
        yahoo.com.mx
        inbox.ru
        inbox.ua
        yahoo.in
        anobic.com
        yahoo.ie
        gmail.com.au
        mail.ru
        yahoo.it
        yahoo.se
        yahoo.com.hk
        ig.com.br
        yahoo.com.br
        yahoo.ca
        uclc.ca
        yahoo.es
        yahoo.gr
        y7mail.com
        yahoo.nl
        bk.ru
        ymail.com
        yahoo.ro
        cs.com
        yahoo.com.tw
        mail.ua
        list.ru
        list.ua
        yahoo.com.au
        yahoo.co.za
        rocketmail.com
        smartcomfort.solutions
        yahoo.dk
        yahoo.fr
        yahoo.com.vn
        vk.com
        yahoo.de
        aim.com
        lookout.com
        my.com
        techsideline.com
        yahoo.com.ar
        yahoo.no
        ccc-ct.com
        class365.ru
        footfallcam.com
        geemail.com
        gmaial.com
        gmaik.com
        gmail.fr
        gmail.org
        gmal.com
        gmail.com
        kp.ru
        nanoav.ru
        paypal.com
        quiltmyshirtscom.zendesk.com
        rhyta.com
        shop.robokassa.ru
        work.cleverbridge.swiss
        yahoo.co.id
        yahoo.co.in
        yahoo.co.nz
        yahoo.com.my
        yahoo.net
        zkgourmet.com
        healthy.co.uk
        gmail.co.uk
        shop.robokassa.ru
        yahoo.com.ph
        bluemonkee.com
        libero.it
        conservatorscenter.org
        acbc.wa.edu.au
        teleworm.us
        googlemail.com
        talktalk.net
        yandex.ru
        ya.ru
        yandex.kz
        yandex.ua
        yandex.com
        yandex.by
      ].freeze

    def initialize(address)
      @address = address
    end

    # @note Used in EmailDataComposer::DataMerger
    # @example
    #   EmailDataComposer::AddressChecker.new('hello@gmail.com').call
    # @return [String]
    #   the same address if its allowed,
    #   default address if not

    def call
      return 'notifications@oos-flint.heroku.com' if
        FORBIDDEN_EMAILS_LIST.any? do |domain|
          @address.include?(domain)
        end

      @address
    end
  end
end
