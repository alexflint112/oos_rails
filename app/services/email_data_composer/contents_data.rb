# frozen_string_literal: true

module EmailDataComposer
  # Prepares the sunject and the content of email notification
  # @attr [Hash] store_setting the settings of the store in app
  # @attr [String] customer_name name of the custmer
  class ContentsData
    def initialize(store_setting, customer_name)
      @store_setting  = store_setting
      @customer_name  = customer_name
    end

    # Returns hash that contains content and subject for email
    # @return [Hash]

    def call
      result = @store_setting.slice(:content, :subject)
      result.each do |_key, value|
        check_for_name(value)
      end
      result
    end

    private

    # Replaces calls for customerName in the string with the name of the customers
    # @param [String] string to be checked
    # @return [String]

    def check_for_name(string)
      string.gsub!('{customerName}', @customer_name)
    end
  end
end
