# frozen_string_literal: true

module EmailDataComposer
  # Returns data of the base product for email
  # @attr [Hash] product_data the data of the product returned by Ecwid
  # @attr [Hash] merchant_settigns the settings of the store in Ecwid
  # @see
  #   MerchantData Merchant Data class
  class BaseProductDataBuilder
    def initialize(product_data, merchant_settings)
      @product_data      = product_data
      @merchant_settings = merchant_settings
    end

    # Returns the hash of needed product data
    # @example
    #   EmailDataComposer::BaseProductDataBuider.new(product_data, merchant_settings).call

    def call
      price_num = @product_data['price']
      {
        'image_url' => @product_data['image_url'],
        'title' => @product_data['title'],
        'product_url' => @product_data['product_url'],
        'price' => product_price(price_num)
      }
    end

    private

    attr_accessor :merchant_settings

    # Ecwid provides price of a product as int
    # Currency prefix and suffix are provided in merchant settings
    # @param price [Integer] price as int
    # @return [String] price with suffix and prefix
    # @example
    #   price(100)
    #   #> $100 USD

    def product_price(price)
      merchant_settings['currency_prefix'] +
        price.to_s +
        merchant_settings['currency_suffix']
    end
  end
end
