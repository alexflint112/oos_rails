# frozen_string_literal: true

module NotificationHandler
  # Returns IDs of all combinations
  # @attr [Hash] combinations All combinations data (data from product_data of Ecwid)
  class ProductCombinations
    def initialize(combinations)
      @combinations = combinations
    end

    # Returns array of combination IDs
    # @return [<Integer>] combination IDs
    def call
      @combinations.map { |combo| combo['id'] }
    end
  end
end
