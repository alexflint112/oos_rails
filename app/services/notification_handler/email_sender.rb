# frozen_string_literal: true

module NotificationHandler
  # Sends emails to the approved requests (for product/combination that is in stock)
  # And deletes requests that are not relevant anymore
  # Is called in webhooks controller (for 'product.updated' webhook from Ecwid)
  # and in storefront controller (when a merchant enables notifications)
  # @attr [Integer] store_id ID of the store
  # @attr [Integer] product_id ID of the product
  class EmailSender
    def initialize(store_id, product_id)
      @store_id          = store_id
      @product_id        = product_id
      @store_setting     = StoreSetting.find_by(store_id: store_id)
      ecwid_data         = MerchantData.new(store_id, product_id)
      @product_data      = ecwid_data.product_data
      @merchant_settings = ecwid_data.merchant_settings
    end

    # Starts processing if store has notifications enabled
    # @example
    #   NotificationHandler::EmailSender.new(store_id, product_id).call

    def call
      process_emails if @store_setting.notifications_enabled?
    end

    private

    # Calls deleter of not relevant requests
    # Then gets all the requests, that should be handled
    # (combinations are in stock)
    # and sends notification

    def process_emails
      delete_requests_for_removed_combinations
      requests = Request.where(
        store_id: @store_id,
        product_id: @product_id,
        combination_id: combinations_in_stock
      )
      requests.each do |request|
        send_email_and_delete(request)
      end
    end

    # Deletes requests for combinations, that no longer exist in product
    # @see
    #   RequestsHandler::DeleteRequestsForRemovedCombos

    def delete_requests_for_removed_combinations
      RequestsHandler::DeleteRequestsForRemovedCombos.new(
        @store_id,
        @product_id,
        product_combinations
      ).call
    end

    # sends email and removes the request after successful sending

    def send_email_and_delete(request)
      email_data = EmailDataComposer::DataMerger.new(
        @store_id,
        @product_id,
        @store_setting,
        @product_data,
        @merchant_settings,
        request
      ).call
      request.delete if deliver_mail(email_data)
    end

    # Creates array of all combination IDs of the product
    # @return [<Integer>] IDs of all combinations
    # @see
    #   NotificationHandler::ProductCombinations

    def product_combinations
      NotificationHandler::ProductCombinations.new(
        @product_data['combinations']
      ).call
    end

    # Creates array of all combination IDs of the product
    # @return [<Integer>] IDs of all combinations
    # @see
    #   NotificationHandler::CombinationsInStock

    def combinations_in_stock
      NotificationHandler::CombinationsInStock.new(
        @product_data
      ).call
    end

    # Send email with the generated email data
    # @param [Hash] email_data data for the email
    # @see
    #   EmailDataComposer::DataMerger

    def deliver_mail(email_data)
      NotificationMailer.back_in_stock_notification(email_data).deliver_now
    end
  end
end
