# frozen_string_literal: true

module NotificationHandler
  # Returns ids of product combinations that are in stock
  # @attr [Hash] product_data the data of the product from Ecwid
  class CombinationsInStock
    def initialize(product_data)
      @product_data = product_data
    end

    # Iterates through all quantity fields of prduct data
    # for both base product and combinations
    # and returns array of combination IDs that are in stock
    # 1 is base product
    # @return [Hash] array of combinations in stock

    def call
      result = []
      @product_data['combinations'].each do |combo|
        in_stock = combo['quantity'].nil? || !combo['quantity'].zero?
        result.push(combo['id']) if in_stock
      end
      base_qty = @product_data['quantity']
      result.push(1) if base_qty.nil? || !base_qty.zero?
      result
    end
  end
end
