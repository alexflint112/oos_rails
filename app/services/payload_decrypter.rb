# frozen_string_literal: true

# Decrypts Ecwid payload. It is passed as a GET parameter in Control Panel
# @see https://developers.ecwid.com/api-documentation/authentication-in-native-apps#payload Ecwid documentation
class PayloadDecrypter
  def initialize(payload)
    @payload = payload
  end

  # authenticate user in iframe page
  def call
    client_secret = 'V81zWUV2LT7E1Q2fFzuz1nuSVC85zrQF'
    # Get the encryption key (16 first bytes of the app's client_secret key).
    encryption_key = client_secret[0..15]
    # Decrypt payload
    aes_128_decrypt(encryption_key, @payload)
  end

  private

  # decrypts payload
  # @return [Hash] the data of the store including ID and tokens
  def aes_128_decrypt(encryption_key, data)
    # Ecwid sends data in url-safe base64. Convert the raw data to the original base64 first
    base64_original = data.gsub(/[\-\_]/, '-' => '+', '_' => '/')
    # Get binary data
    decoded = Base64.decode64(base64_original)
    # Initialization vector is the first 16 bytes of the received data
    iv = decoded[0..15]
    # The payload itself is is the rest of the received data
    payload = decoded[16..-1]
    # decrypt payload
    cipher = OpenSSL::Cipher::AES.new('128-CBC')
    cipher.decrypt
    cipher.key = encryption_key
    cipher.iv = iv
    decrypted_plain_text = cipher.update(payload) + cipher.final
    JSON.parse(decrypted_plain_text)
  end
end
