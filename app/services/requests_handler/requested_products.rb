# frozen_string_literal: true

module RequestsHandler
  # Returns IDs of all products requested in this store
  # @attr [Integer] store_id ID of the store
  class RequestedProducts
    def initialize(store_id)
      @store_id = store_id
    end

    # Returns array of all product IDs that are requested for this store
    # @return [<Integer>] product IDs
    def call
      Request.where(store_id: @store_id).distinct.pluck(:product_id)
    end
  end
end
