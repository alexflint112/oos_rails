# frozen_string_literal: true

module RequestsHandler
  # deletes all the requests  for specified product/store
  class DeleteByStoreAndProduct
    # @param [Integer] store_id   ID of the store
    # @param [Integer] product_id ID of the product
    def initialize(store_id, product_id)
      @store_id   = store_id
      @product_id = product_id
    end

    # Deletes the requests
    def call
      requests = Request.where(store_id: @store_id, product_id: @product_id)
      Request.delete(requests)
    end
  end
end
