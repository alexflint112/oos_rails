# frozen_string_literal: true

module RequestsHandler
  # Removes requests for combinations that were deleted by merchants
  # It is not necessary to send notifications for such requests
  # @attr [Integer] store_id   ID of the store
  # @attr [Integer] product_id ID of the product
  # @attr [Hash] combinations data of all combinations
  class DeleteRequestsForRemovedCombos
    def initialize(store_id, product_id, combinations)
      @store_id     = store_id
      @product_id   = product_id
      @combinations = combinations
    end

    # Deleetes all the requests for combinations that no longer exist
    def call
      # getting the array of combination_ids that no longer exist in product.
      requested = requested_combinations
      if requested
        removed =
          requested -
          @combinations
      end
      # removing the requests for combinations that no longer exist (if any)
      unless removed.empty?
        Request.delete(Request.where(
                         store_id: @store_id,
                         product_id: @product_id,
                         combination_id: removed
                       ))
      end
    end

    private

    # Returns array of all combination IDs that are requested for this product
    # @return [<Integer>] combination IDs
    def requested_combinations
      Request.where(
        'store_id = ? AND product_id = ? AND combination_id > ?',
        @store_id,
        @product_id,
        1
      ).pluck(:combination_id).uniq
    end
  end
end
