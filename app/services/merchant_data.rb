# frozen_string_literal: true

# The class handles API requests to Ecwid to get store data (products, settings)
# @attr [Integer] store_id   ID of the store
# @attr [Integer] product_id ID of the product
# @see
#   https://developers.ecwid.com/api-documentation/rest-api-reference Ecwid REST API
class MerchantData
  require 'http'
  require 'json'

  def initialize(store_id, product_id)
    @store_id = store_id
    @product_id = product_id
    @token = StoreSetting.find_by(store_id: store_id).token
  end

  # Filters merchant settings to leave only the data that we need in app
  # @return [Hash] store data
  def merchant_settings
    unfiltered_settings = full_merchant_settings
    {
      'to_address' => unfiltered_settings['mailNotifications']['customerNotificationFromEmail'],
      'currency_prefix' => unfiltered_settings['formatsAndUnits']['currencyPrefix'],
      'currency_suffix' => unfiltered_settings['formatsAndUnits']['currencySuffix']
    }
  end

  # Filters product data to leave only the data that we need in app
  # @return [Hash] product data
  def product_data
    unfiltered_data = full_product_data
    {
      'image_url' => unfiltered_data['smallThumbnailUrl'],
      'title' => unfiltered_data['name'],
      'product_url' => unfiltered_data['url'],
      'price' => unfiltered_data['price'],
      'combinations' => unfiltered_data['combinations']
    }
  end

  private

  # Gets all the settings of an Ecwid store
  # @return [Hash] settings of store in Ecwid
  # @see
  #   https://developers.ecwid.com/api-documentation/store-information#get-store-profile Ecwid API reference
  def full_merchant_settings
    path = 'https://app.ecwid.com/api/v3/' +
           @store_id.to_s +
           '/profile?token=' +
           @token
    result = HTTP.get(path).body
    JSON.parse(result)
  end

  # Gets all data of the specified product
  # @return [Hash] product data
  # @see
  #   https://developers.ecwid.com/api-documentation/products#get-a-product Ecwid API reference
  def full_product_data
    path = 'https://app.ecwid.com/api/v3/' +
           @store_id.to_s +
           '/products/' +
           @product_id.to_s +
           '?token=' +
           @token
    result = HTTP.get(path).body
    JSON.parse(result)
  end
end
