# frozen_string_literal: true

# Request model
# combination_id default is 1
# 1 means that this is the base product
class Request < ApplicationRecord
  belongs_to :store_setting, foreign_key: 'store_id', primary_key: 'store_id'
  validates :store_id,
            presence: true,
            length: { maximum: 14 }
  validates :name,
            presence: true,
            length: { maximum: 14 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i.freeze
  validates :email,
            presence: true,
            length: { maximum: 50 },
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: {
              scope: %i[product_id combination_id],
              message: 'Request should be unique',
              case_sensitive: false
            }
  validates :product_id,
            presence: true,
            length: { maximum: 14 }
  validates :combination_id,
            presence: true,
            length: { maximum: 14 }
end
