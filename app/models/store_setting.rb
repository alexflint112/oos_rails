# frozen_string_literal: true

# Store settings model. Stores application settings for stores
class StoreSetting < ApplicationRecord
  has_many :requests, foreign_key: 'store_id', primary_key: 'store_id'
  validates :store_id,
            presence: true,
            uniqueness: true,
            length: { maximum: 14 }
  validates :subject,
            presence: true,
            length: { maximum: 80 }
  validates :content,
            presence: true,
            length: { maximum: 255 }
  validates :token,
            presence: true
end
