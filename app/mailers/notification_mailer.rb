# frozen_string_literal: true

# Sends a notificaiton for a product, that is back in stock.
# The data for the email is generated in services/notification_handler/email_data_composer.rb
class NotificationMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notification_mailer.back_in_stock_notification.subject
  #
  def back_in_stock_notification(email_data)
    @email_data = email_data
    mail(
      to: email_data['to'],
      from: email_data['from'] || 'test@example.com',
      subject: email_data['subject']
    )
  end
end
