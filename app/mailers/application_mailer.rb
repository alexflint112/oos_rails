# frozen_string_literal: true

# ApplicationMailer
class ApplicationMailer < ActionMailer::Base
  default from: 'notifications@oos-flint.heroku.com'
  layout 'mailer'
end
