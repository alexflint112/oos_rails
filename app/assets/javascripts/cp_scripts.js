$(document).ready( function() {
  $("#storefrontcheck").change(
    function() {
      sendStorefrontSetting();
    }
  );
  $("#notificationscheck").change(
    function() {
      sendNotificationsSetting();
    }
  );
  $('#btn-revert').click( 
    function() {
      $(".fieldset").removeClass('has-error');
      revertToDefault();
    }
  )}
);


function sendStorefrontSetting() {

  let storefront_enabled = +$("#storefrontcheck").prop("checked");
  let data = {store_setting: {storefront_enabled: storefront_enabled}};

  sendStoreSettings(controller_url, data);
};

function sendNotificationsSetting() {
  
  let notifications_enabled = +$("#notificationscheck").prop("checked");
  let data = {store_setting: {notifications_enabled: notifications_enabled}};

  sendStoreSettings(controller_url, data);
};

function sendStoreSettings(path, params) {
    method = "PUT"; // Set method to post by default if not specified.

    $.ajax({
      type: method,
      url: path,
      data: params,
      dataType: 'json',
      success: function() {
        console.log("Flint: POST request executed");
      },
      crossDomain: true,
      statusCode: {
        200: function() {
          console.log('Flint: settings sent successfully');
        },
        512: function() {
          console.log('Flint: error occurred when trying to save store settings.');
        }
      }
    });
}; // This function executes a request (POST by default) to the needed path with the needed parameters

function revertToDefault() {
  $("#store_setting_subject").val("The product is back in stock");
  $("#store_setting_content").val("Hello {customerName},\n\nThe product that you requested is back in stock. Click the link below to order it!");
  $(".field").each( function() {
    if(!$( this ).hasClass("field--filled")){
       $( this ).addClass("field--filled");
    }}
  );
} // reverts the subj and text settings to default


function validateFields() {
  if ($("#store_setting_subject").val() == "") {
    $("#store_setting_subject").addClass('has-error');
  };
  if ($("#store_setting_content").val() == "") {
    $("#store_setting_content").addClass('has-error');
  };
}