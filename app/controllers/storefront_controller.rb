# frozen_string_literal: true

# Returns the JavaScript code for the application on the storefront
class StorefrontController < ApplicationController
  skip_before_action :verify_authenticity_token

  # Returns JS code for new Ecwid storefront
  def application
    respond_to(:js)
  end

  # Returns JS code for old Ecwid storefront
  def old_application
    respond_to(:js)
  end
end
