# frozen_string_literal: true

# Receives request from storefront applicaiton and saves them in database
class RequestsController < ApplicationController
  skip_before_action :verify_authenticity_token
  after_action :cors_set_access_control_headers

  # Creates a request
  def create
    request = Request.new(request_params)
    head :ok if request.save
  end

  private

  # Params of the request
  def request_params
    params.require(:request).permit(
      :store_id,
      :name,
      :email,
      :product_id,
      :combination_id
    )
  end
end
