# frozen_string_literal: true

# Application controller
class ApplicationController < ActionController::Base
  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST'
  end
end
