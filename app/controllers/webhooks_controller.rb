# frozen_string_literal: true

# Dispatches webhooks and calls the corresponding services
class WebhooksController < ApplicationController
  skip_before_action :verify_authenticity_token
  after_action :cors_set_access_control_headers

  # Calls corresponding service depending on what event is returned by the webhook
  # 1) If a product is updated, checks whether there are any requests for the product
  #    and sends notifications for requests, which entity is in stock
  # 2) If a product is deleted, removes all the
  # @note
  #   in this context entityId of Ecwid's webhook is product ID
  #   in case of "application.installed" webhook it doesn't refer to product
  #   but we don't use it in this case
  # @see
  #   https://developers.ecwid.com/api-documentation/webhooks Ecwid documentation
  def dispatcher
    head :ok
    webhook     = webhook_params
    store_id    = webhook[:storeId]
    product_id  = webhook[:entityId]
    if webhook[:eventType] == 'product.updated'
      if product_requested?(store_id, product_id)
        NotificationHandler::EmailSender.new(store_id, product_id).call
      end
    elsif webhook[:eventType] == 'product.deleted'
      RequestsHandler::DeleteByStoreAndProduct.new(
        webhook[:storeId],
        webhook[:entityId]
      ).call
    end
  end

  private

  # params permitted for the webhook
  def webhook_params
    params.permit(
      :storeId,
      :entityId,
      :eventType
    )
  end

  # Checks, whether there are any requests for the product
  def product_requested?(store_id, product_id)
    Request.exists?(store_id: store_id, product_id: product_id)
  end
end
