# frozen_string_literal: true

# lib/tasks/test_tasks.rake

require 'rails/test_unit/runner'

namespace :test do
  task services: 'test:prepare' do
    $LOAD_PATH << 'test'
    test_files = FileList['test/services/*_test.rb']
    Rails::TestUnit::Runner.run(test_files)
  end
end
