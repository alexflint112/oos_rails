# frozen_string_literal: true

Rails.application.routes.draw do
  get 'store_settings/show'
  get 'store_settings/storefront_setting'
  resources :store_settings, only: :update
  post  'webhooks/dispatcher'
  post  'requests/create'
  get   'requests/delete' # to be edited

  get   '/application',     to: 'storefront#application'
  get   '/old_application', to: 'storefront#old_application'
end
