# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'minitest/reporters'
Minitest::Reporters.use!

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  include FactoryBot::Syntax::Methods
  Faker::Config.locale = 'en-US'

  # Add more helper methods to be used by all tests here...

  def encrypt_payload(store_setting)
    string = "{\"store_id\":#{store_setting.store_id},"\
  					 "\"access_token\":\"#{store_setting.token}\""\
  					 ',"public_token":"public_PNretq6fqgJyGcxHGVvxAmDKWtPEcjcy",'\
  					 '"view_mode":"PAGE","lang":"en"}'
    # AES-128 encrypt (using the app's encryption key)
    cipher = OpenSSL::Cipher::AES.new('128-CBC')
    cipher.encrypt
    cipher.key = 'V81zWUV2LT7E1Q2f'
    iv = cipher.random_iv
    cipher_text = cipher.update(string) + cipher.final
    encrypted_payload = iv + cipher_text
    encoded = Base64.encode64(encrypted_payload)
    encoded.gsub!(%r{[\+/]}, '+' => '-', '/' => '_')
  end
end
