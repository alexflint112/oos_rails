# frozen_string_literal: true

require 'test_helper'

class StoreSettingsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @existing_store = create(:store_setting)
  end

  test 'should not create settings for existing store' do
    payload = encrypt_payload(@existing_store)
    assert_no_difference 'StoreSetting.count' do
      get store_settings_show_path,
          params: {
            payload: payload
          }
    end
  end

  test 'should update token' do
    edited_store = @existing_store.dup
    edited_store.token = Faker::Crypto.md5
    get store_settings_show_path,
        params: {
          payload: encrypt_payload(edited_store)
        }
    assert @existing_store.reload.token == edited_store.token
  end

  test 'should create new settigns for new store' do
    new_store = build(:store_setting)
    assert_difference 'StoreSetting.count', 1 do
      get store_settings_show_path,
          params: {
            payload: encrypt_payload(new_store)
          }
    end
  end

  test 'should update settings' do
    new_data = build(
      :store_setting,
      store_id: @existing_store.store_id
    )
    patch store_setting_path(@existing_store), params: {
      store_setting: {
        storefront_enabled: new_data.storefront_enabled
      }
    }
    assert_equal @existing_store.reload.storefront_enabled,
                 new_data.storefront_enabled
    patch store_setting_path(@existing_store), params: {
      store_setting: {
        notifications_enabled: new_data.notifications_enabled
      }
    }
    assert_equal @existing_store.reload.notifications_enabled,
                 new_data.notifications_enabled
    patch store_setting_path(@existing_store), params: {
      store_setting: {
        subject: new_data.subject,
        content: new_data.content
      }
    }
    assert_equal @existing_store.reload.subject, new_data.subject
    assert_equal @existing_store.reload.content, new_data.content
  end

  test 'hithere' do
    patch store_setting_path(@existing_store), params: {
      store_setting: {
        notifications_enabled: true
      }
    }
  end
end
