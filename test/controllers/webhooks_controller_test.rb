# frozen_string_literal: true

require 'test_helper'

class WebhooksControllerTest < ActionDispatch::IntegrationTest
  def setup
    @store = create(:store_setting)
    @requests = create_list(
      :request, 4, product_id: Faker::Number.number(10), store_setting: @store
    )
  end

  test 'should delete all requests for certain product' do
    store_id = @requests.sample.store_id
    product_id = @requests.sample.product_id
    assert_difference 'Request.count', -4 do
      post webhooks_dispatcher_path, params: {
        eventType: 'product.deleted',
        storeId: store_id,
        entityId: product_id
      }
    end
  end
end
