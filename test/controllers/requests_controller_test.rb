# frozen_string_literal: true

require 'test_helper'

class RequestsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @store = create(:store_setting)
  end

  test 'successful request adding' do
    assert_difference 'Request.count', 1 do
      post requests_create_path, params: {
        request: {
          store_id: @store.store_id,
          name: Faker::Name.first_name,
          email: Faker::Internet.email,
          product_id: Faker::Number.number(10),
          combination_id: Faker::Number.number(10)
        }
      }
    end
  end
end
