# frozen_string_literal: true

FactoryBot.define do
  factory :store_setting do
    store_id              { Faker::Number.number(10) }
    storefront_enabled    { [true, false].sample }
    notifications_enabled { [true, false].sample }
    subject               { Faker::Lorem.sentence(2) }
    content               { Faker::Friends.quote }
    token                 { Faker::Crypto.md5 }

    trait :real do # To be replaced with test store data
      store_id              { 14_096_009 }
      notifications_enabled { true }
      token                 { 'secret_wmGjweDmUL9C3rezjYt6pkQJ7A8EFgkD' }
    end
  end

  factory :request do
    store_id       { Faker::Number.number(10) }
    name           { Faker::Name.first_name   }
    email          { Faker::Internet.email    }
    product_id     { Faker::Number.number(10) }
    combination_id { Faker::Number.number(10) }

    trait :real do
      store_id       { 14_096_009 }
      product_id     { 131_347_805 }
      combination_id { 28_022_404 }
      store_setting  { create(:store_setting, :real) }
    end

    trait :base_product do
      combination_id { 1 }
    end

    store_setting
  end
end
