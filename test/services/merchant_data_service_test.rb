# frozen_string_literal: true

require 'test_helper'

class MerchantDataServiceTest < ActionDispatch::IntegrationTest
  def setup
    create(:store_setting, :real)
    # Product id (2nd argument) should be edited
    # when moving to a real test store
    @data_handler = MerchantData.new(
      14_096_009,
      131_347_805
    )
  end

  test 'should get store settings' do
    data = @data_handler.merchant_settings
    assert_equal data['to_address'], 'flint+oos23@gmail.com'
    assert_equal data['currency_suffix'], 'р.'
  end

  test 'should get product' do
    data = @data_handler.product_data
    assert_equal data['title'], "Automated Tests Product. Don't touch this"
    assert_equal data['combinations'].count, 4
  end
end
