# frozen_string_literal: true

require 'test_helper'

class DeleteRequestsForRemovedCombosTest < ActionDispatch::IntegrationTest
  def setup
    @request = create(:request, :real)
    data_handler = MerchantData.new(
      14_096_009,
      131_347_805
    )
    @product_data       = data_handler.product_data
    @combinations       = NotificationHandler::ProductCombinations.new(
      @product_data['combinations']
    ).call
    create_list(
      :request,
      3,
      product_id: @request.product_id,
      store_setting: @request.store_setting
    )
  end

  test 'remove deleted combinations' do
    assert_difference 'Request.count', -3 do
      RequestsHandler::DeleteRequestsForRemovedCombos.new(
        @request.store_id,
        @request.product_id,
        @combinations
      ).call
    end
  end
end
