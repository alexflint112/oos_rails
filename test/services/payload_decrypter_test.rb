# frozen_string_literal: true

require 'test_helper'

class PayloadDecrypterTest < ActionDispatch::IntegrationTest
  def setup
    @store = create(:store_setting)
    @payload = encrypt_payload(@store)
  end

  test 'should decrypt payload' do
    decrypted = PayloadDecrypter.new(@payload).call
    assert_equal @store.store_id, decrypted['store_id']
    assert_equal @store.token,    decrypted['access_token']
  end
end
