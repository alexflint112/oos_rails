# frozen_string_literal: true

require 'test_helper'

class EmailSenderTest < ActionDispatch::IntegrationTest
  def setup
    @request           = build(:request, :real)
    data_handler       = MerchantData.new(
      14_096_009,
      131_347_805
    )
    @product_data      = data_handler.product_data
    @merchant_settings = data_handler.merchant_settings
    @processor         = NotificationHandler::EmailSender.new(
      14_096_009,
      131_347_805
    )
  end

  test 'should delete removed combinations' do
    create_list(
      :request,
      3,
      :real,
      combination_id: Faker::Number.number(5),
      store_setting: @request.store_setting
    )
    assert_difference 'Request.count', -3 do
      @processor.call
    end
  end
end
