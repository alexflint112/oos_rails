# frozen_string_literal: true

require 'test_helper'

class EmailDataComposerTest < ActionDispatch::IntegrationTest
  def setup
    @request = build(:request, :real)
    data_handler = MerchantData.new(
      14_096_009,
      131_347_805
    )
    @product_data      = data_handler.product_data
    @merchant_settings = data_handler.merchant_settings
  end

  test 'compose for combination' do
    store = @request.store_setting
    data = EmailDataComposer::DataMerger.new(
      14_096_009,
      @request.product_id,
      store,
      @product_data,
      @merchant_settings,
      @request
    ).call
    assert_equal data['to'], @request.email
    assert_equal data['subject'], store.subject
    assert_equal data['title'], "Automated Tests Product. Don't touch this"
    assert_equal data['price'], '1р.'
    assert_not   data['options'].nil?
    assert_equal data['product_url'], 'https://store14096009.ecwid.com/'\
		                          'Automated-Tests-Product-Dont-'\
		                          'touch-this-p131347805'
  end

  test 'compose for base product' do
    store = @request.store_setting
    @request.combination_id = 1
    data = EmailDataComposer::DataMerger.new(
      14_096_009,
      @request.product_id,
      store,
      @product_data,
      @merchant_settings,
      @request
    ).call
    assert_equal data['to'], @request.email
    assert_equal data['subject'], store.subject
    assert_equal data['title'], "Automated Tests Product. Don't touch this"
    assert_equal data['price'], '1345р.'
    assert       data['options'].nil?
    assert_equal data['product_url'], 'https://store14096009.ecwid.com/'\
		                          'Automated-Tests-Product-Dont-'\
		                          'touch-this-p131347805'
  end
end
