# frozen_string_literal: true

require 'test_helper'

class DeleteByStoreAndProductTest < ActionDispatch::IntegrationTest
  def setup
    @request = create(:request)
    create_list(
      :request,
      5,
      product_id: @request.product_id,
      store_setting: @request.store_setting
    )
  end

  test 'remove all requests for the product' do
    assert_difference 'Request.count', -6 do
      RequestsHandler::DeleteByStoreAndProduct.new(
        @request.store_id,
        @request.product_id
      ).call
    end
  end
end
