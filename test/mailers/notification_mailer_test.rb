# frozen_string_literal: true

require 'test_helper'

class NotificationMailerTest < ActionMailer::TestCase
  test 'back in stock' do
    store_setting     = create(:store_setting, :real)
    request           = build(:request, :real, store_setting: store_setting)
    ecwid_data        = MerchantData.new(
      request.store_id,
      request.product_id
    )
    product_data      = ecwid_data.product_data
    merchant_settings = ecwid_data.merchant_settings
    email_data = EmailDataComposer::DataMerger.new(
      request.store_id,
      request.product_id,
      request.store_setting,
      product_data,
      merchant_settings,
      request
    ).call
    mail = NotificationMailer.back_in_stock_notification(email_data)
    assert_equal email_data['subject'], mail.subject
    assert_equal [request.email], mail.to
    assert_match "Automated Tests Product. Don't touch this",
                 mail.body.encoded
  end
end
