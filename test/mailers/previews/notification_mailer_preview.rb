# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/notification_mailer
class NotificationMailerPreview < ActionMailer::Preview
  include FactoryBot::Syntax::Methods
  # Preview this email at http://localhost:3000/rails/mailers/notification_mailer/back_in_stock_notification
  def back_in_stock_notification
    store_setting = StoreSetting.find_by(store_id: 14_096_009)
    request = build(:request, :real, store_setting: store_setting)
    ecwid_data = MerchantData.new(
      request.store_id,
      request.product_id
    )
    product_data      = ecwid_data.product_data
    merchant_settings = ecwid_data.merchant_settings
    email_data = EmailDataComposer::DataMerger.new(
      request.store_id,
      request.product_id,
      request.store_setting,
      product_data,
      merchant_settings,
      request
    ).call
    NotificationMailer.back_in_stock_notification(email_data)
  end

  # Base product preview (it also doesn't have any image)
  # Preview this email at http://localhost:3000/rails/mailers/notification_mailer/back_in_stock_notification_base
  def back_in_stock_notification_base
    store_setting = StoreSetting.find_by(store_id: 14_096_009)
    request = build(
      :request,
      :real,
      :base_product,
      store_setting: store_setting
    )
    ecwid_data = MerchantData.new(
      request.store_id,
      request.product_id
    )
    product_data      = ecwid_data.product_data
    merchant_settings = ecwid_data.merchant_settings
    email_data = EmailDataComposer::DataMerger.new(
      request.store_id,
      request.product_id,
      request.store_setting,
      product_data,
      merchant_settings,
      request
    ).call
    NotificationMailer.back_in_stock_notification(email_data)
  end
end
