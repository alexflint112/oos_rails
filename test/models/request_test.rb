# frozen_string_literal: true

require 'test_helper'

class RequestTest < ActiveSupport::TestCase
  def setup
    store = create(:store_setting)
    @request = create(:request, store_setting: store)
  end

  test 'correct request should be valid' do
    assert @request.valid?
  end

  test 'should not accept empty values' do
    @request.name = ''
    assert_not @request.valid?
    @request.reload
    @request.email = ''
    assert_not @request.valid?
    @request.reload
    @request.store_id = ''
    assert_not @request.valid?
    @request.reload
    @request.product_id = ''
    assert_not @request.valid?
    @request.reload
    @request.combination_id = ''
    assert_not @request.valid?
  end

  test 'email should be correct' do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com foo@bar..com]
    invalid_addresses.each do |email|
      @request.email = email
      assert_not @request.valid?, "#{email} should not be valid"
    end
  end

  test 'request should be unique' do
    original_request  = build(:request)
    copy_request      = original_request.dup
    copy_request.name = Faker::Name.first_name

    original_request.save!
    assert_not copy_request.valid?
  end
end
