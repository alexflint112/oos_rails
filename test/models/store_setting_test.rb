# frozen_string_literal: true

require 'test_helper'

class StoreSettingTest < ActiveSupport::TestCase
  def setup
    @store = create(:store_setting)
  end

  test 'valid settings' do
    settings = build(:store_setting)
    assert settings.valid?
  end

  test 'should not accept empty values' do
    @store.store_id = ''
    assert_not @store.valid?
    @store.reload
    @store.token = ''
    assert_not @store.valid?
  end

  test 'store_id should be unique' do
    store_id = @store.store_id
    new_settings = build(:store_setting, store_id: store_id)
    assert_not new_settings.valid?
  end
end
