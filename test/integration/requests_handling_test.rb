# frozen_string_literal: true

require 'test_helper'

class RequestsHandlingTest < ActionDispatch::IntegrationTest
  def setup
    @request = build(:request)
  end

  test 'should create a request with correct data' do
    request = @request
    post requests_create_url,
         params: {
           request: {
             store_id: request.store_id,
             name: request.name,
             email: request.email,
             product_id: request.product_id,
             combination_id: request.combination_id
           }
         }
    request_in_db = Request.find_by(
      store_id: request.store_id,
      email: request.email,
      product_id: request.product_id
    )
    assert_not request_in_db.nil?
    assert_equal request_in_db.store_id, request.store_id
    assert_equal request_in_db.name, request.name
    assert_equal request_in_db.email, request.email
    assert_equal request_in_db.product_id, request.product_id
    assert_equal request_in_db.combination_id, request.combination_id
  end
end
