# frozen_string_literal: true

require 'test_helper'

class NotificationsAfterSettingsUpdateTest < ActionDispatch::IntegrationTest
  def setup
    ActionMailer::Base.deliveries.clear
    @store_setting = create(:store_setting, :real)
    create(
      :request,
      :real,
      store_setting: @store_setting
    )
    create(
      :request,
      :real,
      :base_product,
      product_id: 131_711_857,
      store_setting: @store_setting
    )
  end

  test 'should send 2 mails after notifications are enabled' do
    store_setting = @store_setting
    assert_difference 'Request.count', -2 do
      patch store_setting_path(store_setting), params: {
        store_setting: {
          notifications_enabled: true
        }
      }
    end
    assert_equal 2, ActionMailer::Base.deliveries.size
  end
end
