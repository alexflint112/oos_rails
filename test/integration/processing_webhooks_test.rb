# frozen_string_literal: true

require 'test_helper'

class ProcessingWebhooksTest < ActionDispatch::IntegrationTest
  def setup
    ActionMailer::Base.deliveries.clear
    @request = create(:request, :real)
  end

  test 'should send notifications for combination in stock' do
    request = @request
    post webhooks_dispatcher_url,
         params: {
           eventType: 'product.updated',
           storeId: request.store_id,
           entityId: request.product_id
         }
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert Request.find_by(
      store_id: request.store_id,
      product_id: request.product_id,
      email: request.email
    ).nil?
  end

  test 'should delete all requests for deleted product' do
    create_list(:request, 5, :real, store_setting: @request.store_setting)
    request = @request
    assert_difference 'Request.count', -6 do
      post webhooks_dispatcher_url,
           params: {
             eventType: 'product.deleted',
             storeId: request.store_id,
             entityId: request.product_id
           }
    end
  end
end
