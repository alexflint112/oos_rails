# frozen_string_literal: true

# Creates application settings for store in the database
# Storefront and notifications are disabled by default
# to make sure, that the user sets up the app before using it
# storefront_enabled defines the appearance of the request form in the store
# notifications_enabled defines sending the notifications
class CreateStoreSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :store_settings do |t|
      t.integer :store_id, limit: 8
      t.boolean :storefront_enabled, default: false
      t.boolean :notifications_enabled, default: false
      t.string  :subject,
                default: 'The product is back in stock'
      t.string  :content, default: "Hello {customerName},\n\nThe product"\
                                   ' that you requested is back in stock.' \
                                   ' Click the link below to order it!'
      t.string  :token

      t.timestamps
    end
    add_index :store_settings, :store_id, unique: true
  end
end
