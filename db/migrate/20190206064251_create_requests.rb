# frozen_string_literal: true

# Requests for notifications.
# If combination id == 1, it means, that the request is for base product
class CreateRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :requests do |t|
      t.integer :store_id, limit: 8
      t.string  :name
      t.string  :email
      t.integer :product_id, limit: 8
      t.integer :combination_id, default: 1, limit: 8

      t.timestamps
    end
    add_index :requests, %i[store_id product_id]
    add_index :requests, %i[email product_id combination_id], unique: true
  end
end
