# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_09_062101) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "requests", force: :cascade do |t|
    t.bigint "store_id"
    t.string "name"
    t.string "email"
    t.bigint "product_id"
    t.bigint "combination_id", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email", "product_id", "combination_id"], name: "index_requests_on_email_and_product_id_and_combination_id", unique: true
    t.index ["store_id", "product_id"], name: "index_requests_on_store_id_and_product_id"
  end

  create_table "store_settings", force: :cascade do |t|
    t.bigint "store_id"
    t.boolean "storefront_enabled", default: false
    t.boolean "notifications_enabled", default: false
    t.string "subject", default: "The product is back in stock"
    t.string "content", default: "Hello {customerName},\n\nThe product that you requested is back in stock. Click the link below to order it!"
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_store_settings_on_store_id", unique: true
  end

end
