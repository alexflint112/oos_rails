# frozen_string_literal: true

StoreSetting.create!(store_id: 11_589_526,
                     storefront_enabled: true,
                     notifications_enabled: true,
                     subject: 'Hello there',
                     content: 'General Kenobi',
                     token: 'secret_VwEPvv95PcSGSj9YGSzXyq2bsMBcnBEP')

StoreSetting.create!(store_id: 14_096_009,
                     storefront_enabled: true,
                     notifications_enabled: true,
                     token: 'secret_wmGjweDmUL9C3rezjYt6pkQJ7A8EFgkD')

5.times do |n|
  email = "test+#{n}@example.com"
  Request.create!(store_id: 11_589_526,
                  name: Faker::Name.first_name,
                  email: email,
                  product_id: 111)
end

3.times do |n|
  email = "test+#{n}@example.com"
  Request.create!(store_id: 11_589_526,
                  name: Faker::Name.first_name,
                  email: email,
                  product_id: 111,
                  combination_id: 10 + n)
end

Request.create!(
  store_id: 14_096_009,
  product_id: 108_784_134,
  combination_id: 24_496_133,
  email: 'flint@ecwid.com',
  name: 'Alex'
)

Request.create!(
  store_id: 14_096_009,
  product_id: 108_784_134,
  combination_id: 24_503_000,
  email: 'flint@ecwid.com',
  name: 'Alex'
)

Request.create!(
  store_id: 14_096_009,
  product_id: 108_784_134,
  combination_id: 28_022_403,
  email: 'flint@ecwid.com',
  name: 'Alex'
)

Request.create!(
  store_id: 14_096_009,
  product_id: 108_784_134,
  email: 'flint@ecwid.com',
  name: 'Alex'
)
