# Back in stock 

The application allows customers of Ecwid stores to request a notification when a requested product is back in stock.

The application interacts with Ecwid via [API](https://developers.ecwid.com/api-documentation/)

* [Storefront API](https://developers.ecwid.com/api-documentation/storefront-js-api) to subscribe to storefront events (page load, checking the correct page)
* [Webhooks](https://developers.ecwid.com/api-documentation/webhooks) to receive notifications about product changes
* [REST API](https://developers.ecwid.com/api-documentation/products) to get products' info (after receiving webhook)