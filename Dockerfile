FROM ruby:2.5.1
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
RUN mkdir /oos_app
WORKDIR /oos_app
COPY . /oos_app
COPY ./config/database.dc.yml /oos_app/config/database.yml
RUN bundle install


# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]